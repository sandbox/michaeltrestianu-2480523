This module's aim is to enable developers to be able to create an activity stream that records a sites user actions.

When installed the following table is created with the following columns:

Table name 		- simple_activity_stream
Column names		- activity_stream_id (pk) auto-incremented
					- nid (fk)
					- tid (fk)
					- uid (fk)
					- date (unix timestamp)


This module uses the entity api to create a custom stream entity (note: thinking it should maybe be renamed to an action)

It is therefore compatible with views via the use of the EntityDefaultViewsController class.

This module does not provide any functionality or url's to display activity streams. This can be achieved using Drupal's powerful views module.

Seperate stream entities can be edited, deleted and viewed via the implementation of the EntityAPIController and entity classes which have been extended using this modules streamEntityController class and stream class.

To manage activity streams simply use the following url admin/activity_stream.

To view a single stream entity use the following url activity_stream/id/% - This uses a menu hook within this module which displays the entity's values. It displays the data linked to its related content.

The module has been built so that an activity type is a taxonomy term. This was done so that categorisation of activties was kept simple and made in-line with Drupals core functionality.

The user responsible for creating the record is recorded in the uid column along with the node associated in the nid column.

The date of which the action occured is recorded as a unixtimestamp string in the date column. When creating a stream entity you should use php's time() functionality

This module can be used via the use of Drupal's hooks. A basic example is shown below on how entities should be created.

	<?php

		function hook_node_delete($node){

			if($node->type == "node_type_here"){

				update_stream($node->nid,$user->uid,'delete');

			}

		}


		function update_stream($nid,$uid,$update_type = NULL){

			$tid = null;

				if($update_type == 'delete'){

					$tid = 8;


				}

			$stream_entity = entity_create('stream', array(

				'nid' => $nid,
				'tid' => $tid,
				'uid' => $uid,
				'date' => time(),

			));

			entity_save('stream', $stream_entity);

		}

That's pretty much it, If you have any questions or feedback then let me know :)