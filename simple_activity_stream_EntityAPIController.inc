<?php
/**
 * @file
 * Class that extends the functionality of the EntityAPIController
 */

/**
 * Extending the EntityAPIController for the stream entity.
 */
class StreamEntityController extends EntityAPIController {

  /**
   * Function overrides EntityAPIController::buildContent().
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    global $base_url;

    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    $build['activty_stream_id'] = array(
      '#type'		=> 'markup',
      '#markup' => check_plain($entity->activity_stream_id),
      '#prefix' => t('Activity id:') . '<div id ="activty_stream_id_' . $entity->activity_stream_id . '">',
      '#suffix' => '</div>',
    );

    $build['nid'] = array(
      '#type'		=> 'markup',
      '#markup' => l(t('Node'), 'node/' . $entity->nid),
      '#prefix' => t('Node associated:') . '<div id ="activty_stream_id_' . $entity->nid . '">',
      '#suffix' => '</div>',
    );

    $build['tid'] = array(
      '#type'		=> 'markup',
      '#markup' => l(t('Taxonomy term'), 'taxonomy/term/' . $entity->tid),
      '#prefix' => t('Taxonomy term associated:') . '<div id ="activty_stream_id_' . $entity->tid . '">',
      '#suffix' => '</div>',
    );

    $build['uid'] = array(
      '#type'		=> 'markup',
      '#markup' => l(t('User'), 'user/' . $entity->uid),
      '#prefix' => t('User associated:') . '<div id ="activty_stream_id_' . $entity->uid . '">',
      '#suffix' => '</div>',
    );

    return $build;
  }

}
