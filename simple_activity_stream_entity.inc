<?php
/**
 * @file
 * Stream entity class extending the Entity class
 */

class Stream extends Entity {

  /**
   * Change the default URI from default/id to activity_stream/id.
   */
  protected function defaultUri() {
    return array('path' => 'activity_stream/id/' . $this->identifier());
  }

}
